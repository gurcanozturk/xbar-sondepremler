#!/usr/local/bin/python3
# <xbar.title>Recent Earthquakes in Turkiye</xbar.title>
# <xbar.version>v0.1</xbar.version>
# <xbar.author>A.Gurcan Ozturk</xbar.author>
# <xbar.author.github>gurcanozturk</xbar.author.github>
# <xbar.desc>Displays last 10 earthquakes in Turkiye, original data from https://www.koeri.boun.edu.tr </xbar.desc>
# <xbar.image>http://i.imgur.com/lF8Qdpk.png</xbar.image>
# <xbar.dependencies>Python3</xbar.dependencies>
# <xbar.abouturl>http://www.koeri.boun.edu.tr</xbar.abouturl>

import requests, json

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'

url = 'https://deprem-api.herokuapp.com'
request = requests.get(url, headers={'User-Agent': user_agent})

def display(request):
   line =""
   data = json.loads(request.text)["depremler"]
   for i, val in enumerate(data):
     if (val["Id"] <= 10):
       tarih = val["Tarih"].replace('.', '/')
       saat  = val["Saat"]
       enlem  = val["Enlem(N)"]
       boylam  = val["Boylam(E)"]
       ml     = val["Buyukluk"]["ML"]
       yer  = val["Yer"]

       line += ("%s-%s  %s %s (%sN, %sE)" % (tarih,saat,ml,yer,enlem,boylam)) + "\n"
    return line.rstrip('\n')

if __name__ == '__main__':
   print ('🌎')
   print ('---')
   print ("Son Depremler - Tarih/Saat, Buyukluk, Yer, Koordinatlar")
   print ('---')
   print (display(request))
