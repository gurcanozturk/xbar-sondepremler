# xbar-sondepremler

Mac OS X xbar plugin written in python, shows last 10 earthquakes in Turkiye at menu bar. 
Python3 modules required; requests, json.

Put python script into $HOME/Library/Application Support/xbar/plugins folder then click Refresh from any xbar logo on menu bar or restart xbar.
